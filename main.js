function createAdjacencyMatrix(nodes,edges) {
	var edgeHash = {};
	for (x in edges) {
		var id = edges[x].source + "-" + edges[x].target;
		edgeHash[id] = edges[x];
	}
	matrix = [];
	//create all possible edges
	for (const [a, node_a] of nodes.entries()) {
		for (const [b, node_b] of nodes.entries()) {
		var grid = {id: node_a.id + "-" + node_b.id, x: a, y: b, sharedfollowers: 0};
		if (edgeHash[grid.id]) {
			grid.sharedfollowers = parseInt(edgeHash[grid.id].sharedfollowers);
		}
		matrix.push(grid);
		}
	}
	return matrix;
}

function getColorClass(sharedfollowers) {
    if (sharedfollowers === 0) {
        return "sharedfollowers-0";
    } else if (sharedfollowers === 1) {
        return "sharedfollowers-1";
    } else {
        return "sharedfollowers-other";
    }
}
  
  
d3.csv("noeuds.csv").then(function (nodes) {
    d3.csv("liens.csv").then(function (edges) {  
        // matrice d'adjacence
        var adjacencyMatrix = createAdjacencyMatrix(nodes, edges);
  
        // couleurs
        var maxSharedFollowers = d3.max(adjacencyMatrix, function (d) { return d.sharedfollowers; });
        var colorScale = d3.scaleQuantize()
            .domain([0, maxSharedFollowers])
            .range(d3.schemeBlues[9]);
  
        // affichage de la matrice d'adjacence
        var matrixElt = d3.select("svg")
            .append("g")
            .attr("transform", "translate(50,50)")
            .attr("id", "adjacencyMatrix");
    
        matrixElt.selectAll("rect")
            .data(adjacencyMatrix)
            .enter().append("rect")
            .attr("x", function (d) { return d.x * 25; })
            .attr("y", function (d) { return d.y * 25; })
            .attr("width", 25)
            .attr("height", 25)
            .attr("class", function (d) { return "matrix-cell " + getColorClass(d.sharedfollowers); })
            .style("fill", function (d) { return colorScale(d.sharedfollowers); });
    
        // axes
        var scaleSize = nodes.length * 25;
    
        var x = d3.scaleBand()
            .domain(nodes.map(function (el) { return el.id; }))
            .range([0, scaleSize]);
    
        var y = d3.scaleBand()
            .domain(nodes.map(function (el) { return el.id; }))
            .range([scaleSize, 0]);
    
        matrixElt.append("g")
            .attr("transform", "translate(0," + scaleSize + ")")
            .call(d3.axisBottom(x))
            .selectAll("text")
            .attr("transform", "rotate(45)")
            .style("text-anchor", "start");
        
    
        matrixElt.append("g")
            .call(d3.axisLeft(y));
    
        // guides visuels
        var guideX = matrixElt.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 25)
            .attr("height", scaleSize)
            .attr("opacity", 0)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 2);
    
        var guideY = matrixElt.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", scaleSize)
            .attr("height", 25)
            .attr("opacity", 0)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", 2);
    
        matrixElt.selectAll("rect")
            .on("mouseover", function (d) {
            guideX.attr("x", d.x * 25).attr("opacity", 1);
            guideY.attr("y", d.y * 25).attr("opacity", 1);
            })
            .on("mouseout", function () {
            guideX.attr("opacity", 0);
            guideY.attr("opacity", 0);
            });
    });
  });  